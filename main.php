<?php
/**
 * Plugin Name:       Geonames Form
 * Description:       Plugin to create a Bootstrap form that utilize Geonames.org API to show a countries list and their cities. Shortcode [ctf-form-geonames]
 * Version:           1.0
 * Author:            Jordi Farre
 * Author URI:        https://jordimaster.github.io/
 * License: GPLv2
 */



// Register Styles and Scripts
function ctf_form_geonames_scripts() {

	wp_enqueue_script( 'geoquery', plugins_url( '/js/geoquery.js', __FILE__ ), array('jquery') );

	wp_enqueue_script( 'ctf-form-scripts', plugins_url( '/js/scripts.js', __FILE__ ), array('jquery') );

	wp_enqueue_style( 'ctf-bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css');

	wp_register_style( 'ctf-form-styles', plugins_url('css/styles.css', __FILE__) );
	wp_enqueue_style( 'ctf-form-styles' );

}
add_action('wp_enqueue_scripts','ctf_form_geonames_scripts');




// Add Shortcode to show the form
function ctf_form_geonames() {
    ob_start();
    ?>

		<div class="ct-form-geonames container border border-dark my-5 py-5 px-lg-5 px-4 rounded bg-light shadow-lg">
			<h2 class="text-center mb-5 pb-3 fw-bold"><u>Geonames Form</u></h2>
			<form class="needs-validation" novalidate method="post">
				<div class="row gx-4">
					<div class="col-md-6 mb-3 ">
						<div class="form-floating">
							<input type="text" class="form-control" id="ctf-name" placeholder="John Doe" required>
							<div class="invalid-feedback">Enter your name</div>
							<label for="ctf-name">Name</label>
						</div>
					</div>
					<div class="col-md-6 mb-3 ">
						<div class="form-floating">
							<input type="email" class="form-control" id="ctf-email" placeholder="name@example.com" required>
							<div class="invalid-feedback">Enter your email</div>
							<label for="ctf-email">Email address</label>
						</div>
					</div>
				</div>
				<div class="row gx-4">
					<div class="col-md-6 mb-3">
						<div class="form-floating">
							<select class="form-select" id="ctf-country" required disabled></select>
							<div class="invalid-feedback">Select a country</div>
							<label for="country">Country</label>
						</div>
					</div>
					<div class="col-md-6 mb-3">
						<div class="form-floating">
							<select class="form-select" id="ctf-city" required disabled></select>
							<div class="invalid-feedback">Select a city</div>
							<label for="city">City</label>
						</div>
					</div>
				</div>
				<div class="form-group mb-5">
					<div class="form-floating">
						<textarea class="form-control" id="ctf-message" placeholder="Write a message" required></textarea>
						<div class="invalid-feedback">Write a message</div>
						<label for="ctf-message">Message</label>
					</div>
				</div>
				<button class="d-block mx-auto btn btn-outline-dark btn-lg fw-bold py-3 px-5 text-capitalize border border-dark text-decoration-none" type="submit">Send</button>
			</form>
		</div>

<?php
    return ob_get_clean();
}
add_shortcode( 'ctf-form-geonames', 'ctf_form_geonames' );

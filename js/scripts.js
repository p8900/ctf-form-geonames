
jQuery(function ($) {

	$("#ctf-country").geoCountrySelect({
		callback: function () {
			$("#ctf-country").removeAttr('disabled');
		}
	});
	$("#ctf-country").change(function(){
		if ($("#ctf-country").val()) {
			$("#ctf-city").geoCitySelect({
				callback: function () {
					$("#ctf-city").removeAttr('disabled');
				},
				code: $("#ctf-country").val()
			});
		}
		else
			$("#ctf-city").attr('disabled', true).html("");
	});

});


(function() {
	'use strict';
	window.addEventListener('load', function() {
		var forms = document.getElementsByClassName('needs-validation');
		var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				}
				form.classList.add('was-validated');
			}, false);
		});
	}, false);
})();
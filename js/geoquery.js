/* 
 * geoQuery v1
 *
 * Retrieve country and city data from geonames.org
 *
 */

(function ($) {

  var username = 'tompi'; // change here geonames username

  $.fn.geoCountrySelect = function (options) {
    var el = this;
    $.when( $.ajax( 'http://api.geonames.org/countryInfoJSON?username='+username ) )
    .then(function (data) {
      var sortedNames = data.geonames;
      if (data.geonames.sort) {
        sortedNames = data.geonames.sort(function (a, b) {
          return a.countryName.localeCompare(b.countryName);
        });
      }
      sortedNames.unshift({countryCode:'', countryName:''});
      var html = $.map(sortedNames, function(c) {
		return '<option value="' + c.countryCode + '">' + c.countryName + '</option>';
      });
      el.html(html);
      if (options && options.callback) options.callback(sortedNames);
    }, function(){alert('Geonames server returned error')});
  };

  $.fn.geoCitySelect = function (options) {
    var el = this;
    $.when( $.ajax( 'http://api.geonames.org/searchJSON?q='+options.code+'&country='+options.code+'&featureCode=PPLC&featureCode=PPLA&username='+username ) )
    .then(function (data) {
      var sortedNames = data.geonames;
      if (data.geonames.sort) {
        sortedNames = sortedNames.sort(function (a, b) {
          return a.name.localeCompare(b.name);
        });
      }
      sortedNames.unshift({toponymName:'', name:''});
      var html = $.map(sortedNames, function(c) {
		return '<option value="' + c.toponymName + '">' + c.name + '</option>';
      });
      el.html(html);
      if (options && options.callback) options.callback(sortedNames);
    }, function(){alert('Geonames server returned error')});
  };

})(jQuery);
